'use strict';

class Operator {
  constructor(evaluateOperator) {
    this.evaluate = evaluateOperator; // signature: evaluate(boundCollection, options, ...args)
  }
}

module.exports = Operator;
