'use strict';

class OperatorExpression {
  constructor({ name, args }) {
    this.name = name;
    this.args = args;
  }
}

module.exports = OperatorExpression;
