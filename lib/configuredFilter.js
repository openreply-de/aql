'use strict';

const { grammar, semantics } = require('./parser');
const operators = require('./operators');
const { createPredicate } = require('./predicate');
const { filter } = require('./filter');

const configuredFilter = filter(grammar, semantics, operators, createPredicate);

module.exports = configuredFilter;
