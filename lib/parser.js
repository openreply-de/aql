'use strict';

const fs = require('fs');
const ohm = require('ohm-js');

const OperatorExpression = require('./operator-expression');

// eslint-disable-next-line security/detect-non-literal-fs-filename
const rawGrammar = fs.readFileSync(`${__dirname}/grammar.ohm`);
const grammar = ohm.grammar(rawGrammar);

const semantics = grammar.createSemantics().addOperation(
  'evaluate',
  {
    Exp: (operatorExp) => operatorExp.evaluate(),
    /* eslint-disable no-unused-vars */
    OperatorExp: (operator, _open, argumentList, _close) => new OperatorExpression({
      name: operator.evaluate(),
      args: argumentList.evaluate()
    }),
    Operator: (letter, word) =>
      `${letter.evaluate()}${word.evaluate().join('')}`,
    ArgumentList: (argument, _comma, argList) =>
      [argument.evaluate(), ...argList.evaluate()],
    Argument_operator: (operatorExp) => operatorExp.evaluate(),
    Argument_word: (word) => word.sourceString.trim().replace(/\\([)(,\\])/g, '$1'),

    // eslint-disable-next-line no-underscore-dangle
    _terminal() {
      return this.sourceString;
    }
    /* eslint-enable no-unused-vars */
  }
);

module.exports = {
  grammar,
  semantics
};
