'use strict';

const Operator = require('./operator');
const AQLError = require('./errors/AQLError');

const evaluateExpression = (operators) => (collection, expression, parentOptions) => {
  const evaluate = evaluateExpression(operators);
  const { name, args } = expression;
  const operator = operators[name]; // eslint-disable-line security/detect-object-injection
  if (!operator) throw new AQLError(`Operator '${name}' not found`);
  if (!(operator instanceof Operator)) throw new AQLError(`Operator '${name}' invalid`);
  return operator.evaluate(evaluate, collection, parentOptions, ...args);
};

const createPredicate = (operators, expression) => (collection) => {
  const options = { bindings: {} };
  return evaluateExpression(operators)(collection, expression, options);
};

module.exports = {
  createPredicate
};
