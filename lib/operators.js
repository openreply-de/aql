'use strict';

const _ = require('lodash');
const jsonpath = require('jsonpath');

const Operator = require('./operator');
const AQLError = require('./errors/AQLError');

const partitionPath = (path) => {
  const parsed = jsonpath.parse(path);
  const first = parsed[0].expression.value;
  const rest = parsed.length > 1 ? jsonpath.stringify(parsed.slice(1)) : null;
  return [first, rest];
};

const queryPath = (item, path, { bindings }) => {
  const [first, rest] = partitionPath(path);
  if (_.has(bindings, first)) return jsonpath.query(bindings[first], rest); // eslint-disable-line security/detect-object-injection
  return jsonpath.query(item, path);
};

const queryModifierScalar = (actualValues, condition) =>
  condition(_.first(actualValues));

const parseNumber = (numberString) =>
  (numberString.includes('.')
    ? parseFloat(numberString)
    : parseInt(numberString, 10));

// the expectedValue is always a String
const typeCast = (actualValue, expectedValue) => {
  if (_.isString(actualValue)) {
    return expectedValue;
  }
  if (_.isNull(actualValue)) {
    return expectedValue.toLowerCase() === 'null' ? null : expectedValue;
  }
  if (_.isBoolean(actualValue)) {
    return expectedValue.toLowerCase() === 'true';
  }
  if (!Number.isNaN(Number(actualValue))) {
    return parseNumber(expectedValue);
  }
  return expectedValue;
};

const test = (actualValues, condition, { queryModifier = queryModifierScalar }) =>
  !_.isEmpty(actualValues) && queryModifier(actualValues, condition);


const quantor = (quantorTestFn) => new Operator((evaluateSubexpression, collection, options, path, bindingName, condition) => {
  if (_.some(_.get(options, ['bindings', bindingName]))) throw new AQLError(`binding already set: ${bindingName}`);
  const filteredCollection = collection.filter((item) => quantorTestFn(queryPath(item, path, options), (boundItem) => {
    options.bindings[bindingName] = boundItem; // eslint-disable-line security/detect-object-injection, no-param-reassign
    const filteredItem = evaluateSubexpression([item], condition, options);
    delete options.bindings[bindingName]; // eslint-disable-line security/detect-object-injection, no-param-reassign
    const conditionMet = _.some(filteredItem);
    return conditionMet;
  }));
  return filteredCollection;
});

const every = (items, predicate) => _.some(items) && _.every(items, predicate);
const all = quantor(every);

const any = quantor(_.some);

const eq = new Operator((evaluateSubexpression, collection, options, path, expectedValue, caseInsensitive = 'false') =>
  collection.filter((item) => {
    const actualValues = queryPath(item, path, options);
    const condition = (actualValue) =>
      actualValue === typeCast(actualValue, expectedValue);
    if (caseInsensitive === 'true') {
      return new RegExp('^' + expectedValue + '$', 'i').test(actualValues, condition, options);
    }
    return test(actualValues, condition, options);
  }));

const ne = new Operator((evaluateSubexpression, collection, options, path, expectedValue) =>
  collection.filter((item) => {
    const actualValues = queryPath(item, path, options);
    const condition = (actualValue) =>
      actualValue !== typeCast(actualValue, expectedValue);
    return test(actualValues, condition, options);
  }));

const inFilter = new Operator((evaluateSubexpression, collection, options, path, ...expectedValues) =>
  collection.filter((item) => {
    const actualValues = queryPath(item, path, options);
    const condition = (actualValue) =>
      expectedValues.map((v) =>
        typeCast(actualValue, v)).includes(actualValue);
    return test(actualValues, condition, options);
  }));

const out = new Operator((evaluateSubexpression, collection, options, path, ...expectedValues) =>
  collection.filter((item) => {
    const actualValues = queryPath(item, path, options);
    const condition = (actualValue) =>
      !expectedValues.map((v) =>
        typeCast(actualValue, v)).includes(actualValue);
    return test(actualValues, condition, options);
  }));

const gt = new Operator((evaluateSubexpression, collection, options, path, expectedValue) =>
  collection.filter((item) => {
    const actualValues = queryPath(item, path, options);
    const condition = (actualValue) =>
      actualValue > typeCast(actualValue, expectedValue);
    return test(actualValues, condition, options);
  }));

const ge = new Operator((evaluateSubexpression, collection, options, path, expectedValue) =>
  collection.filter((item) => {
    const actualValues = queryPath(item, path, options);
    const condition = (actualValue) =>
      actualValue >= typeCast(actualValue, expectedValue);
    return test(actualValues, condition, options);
  }));

const lt = new Operator((evaluateSubexpression, collection, options, path, expectedValue) =>
  collection.filter((item) => {
    const actualValues = queryPath(item, path, options);
    const condition = (actualValue) =>
      actualValue < typeCast(actualValue, expectedValue);
    return test(actualValues, condition, options);
  }));

const le = new Operator((evaluateSubexpression, collection, options, path, expectedValue) =>
  collection.filter((item) => {
    const actualValues = queryPath(item, path, options);
    const condition = (actualValue) =>
      actualValue <= typeCast(actualValue, expectedValue);
    return test(actualValues, condition, options);
  }));

const startsWith = new Operator((evaluateSubexpression, collection, options, path, expectedValue) =>
  collection.filter((item) => {
    const actualValues = queryPath(item, path, options);
    const condition = (actualValue) =>
      actualValue.startsWith(expectedValue);
    return test(actualValues, condition, options);
  }));

const endsWith = new Operator((evaluateSubexpression, collection, options, path, expectedValue) =>
  collection.filter((item) => {
    const actualValues = queryPath(item, path, options);
    const condition = (actualValue) =>
      actualValue.endsWith(expectedValue);
    const conditionMet = test(actualValues, condition, options);
    return conditionMet;
  }));

/**
 * @deprecated since string and array are not handled everywhere the same, includesItem (for array)
 * and contains (for string,number) should be used
 */
const includes = new Operator((evaluateSubexpression, collection, options, path, expectedValue) =>
  collection.filter((item) => {
    const actualValues = queryPath(item, path, options);
    const condition = (actualValue) =>
      ((_.isString(actualValue) || _.isNumber(actualValue))
        && _.toString(actualValue).includes(expectedValue))
      || (_.isArray(actualValue) && _.includes(actualValue, expectedValue));
    return test(actualValues, condition, options);
  }));

const includesItem = new Operator((evaluateSubexpression, collection, options, path, expectedValue) =>
  collection.filter((item) => {
    const actualValues = queryPath(item, path, options);
    const condition = (actualValue) => _.isArray(actualValue) && _.includes(actualValue, expectedValue);
    return test(actualValues, condition, options);
  }));

const contains = new Operator((evaluateSubexpression, collection, options, path, expectedValue) =>
  collection.filter((item) => {
    const actualValues = queryPath(item, path, options);
    const condition = (actualValue) => (_.isString(actualValue) || _.isNumber(actualValue))
      && _.toString(actualValue).includes(expectedValue);
    return test(actualValues, condition, options);
  }));

const and = new Operator((evaluateSubexpression, collection, options, ...subexpressions) => {
  const subexpressionResults = subexpressions.map((e) => evaluateSubexpression(collection, e, options));
  const intersection = _.intersection(...subexpressionResults);
  return intersection;
});

const or = new Operator((evaluateSubexpression, collection, options, ...subexpressions) => {
  const subexpressionResults = subexpressions.map((e) => evaluateSubexpression(collection, e, options));
  const union = _.union(...subexpressionResults);
  return union;
});

module.exports = {
  eq,
  ne,
  in: inFilter,
  out,
  gt,
  ge,
  lt,
  le,
  startsWith,
  endsWith,
  includes,
  includesItem,
  contains,
  and,
  or,
  all,
  any
};
