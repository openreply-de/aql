'use strict';

const _ = require('lodash');
const AQLError = require('./errors/AQLError');

const filter = (grammar, semantics, predefinedOperators, createPredicate) => (conditionString, customOperators = {}) => {
  if (_.isEmpty(conditionString)) return (collection) => collection;

  const match = grammar.match(conditionString);
  if (match.failed()) {
    throw new AQLError(`Condition malformed: ${match.shortMessage}`, _.pick(match, 'message', 'shortMessage'));
  }
  const expression = semantics(match).evaluate();
  const operators = _.assign({}, predefinedOperators, customOperators);
  return createPredicate(operators, expression);
};

module.exports = { filter };
