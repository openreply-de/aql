const crypto = require('crypto');

process.env.NODE_ENV = process.env.NODE_ENV || 'test';
if (!process.env.CHOMA_SEED) {
  process.env.CHOMA_SEED = crypto.randomBytes(6).toString('hex');
}

require('choma');

const path = require('path');

const basedir = path.resolve(__dirname, '../..');
process.env.NODE_PATH = process.env.NODE_PATH || basedir;
process.chdir(basedir);
