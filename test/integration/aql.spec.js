'use strict';

const { expect } = require('chai');
const { AQLError, Operator, filter } = require('../..');

describe('AQL module', () => {
  describe('when there are no custom operators', () => {
    describe('special condition strings', () => {
      const testCollection = ['a', 'b'];

      it('does not filter for undefined', () => {
        const conditionString = undefined;

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal(testCollection);
      });

      it('does not filter for null', () => {
        const conditionString = null;

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal(testCollection);
      });

      it('does not filter for an empty string', () => {
        const conditionString = '';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal(testCollection);
      });

      it('parses escaped characters properly', () => {
        const value = { escapedString: 'c(d,e)\\' };
        const conditionString = 'eq(escapedString, c\\(d\\,e\\)\\\\)';

        const filtered = filter(conditionString)([value]);
        expect(filtered).to.deep.equal([value]);
      });
    });

    describe('inexistent fields', () => {
      const abc = { name: 'abc' };
      const def = { name: 'def' };
      const ghi = { name: 'ghi' };
      const testCollection = [abc, def, ghi];

      it('does not fail', () => {
        const conditionString = 'eq(name1, def)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([]);
      });
    });

    describe('syntax error', () => {
      const testCollection = [];

      it('fails', () => {
        const conditionString = 'eq(';

        expect(() => filter(conditionString)(testCollection)).to.throw(AQLError);
      });
    });
  });

  describe('operators', () => {
    describe('when the actual value is NaN', () => {
      it('does not match', () => {
        const conditionString = 'eq(number, 123)';
        const value = { number: NaN };

        const filtered = filter(conditionString)([value]);
        expect(filtered).to.deep.equal([]);
      });
    });

    describe('when the operator does not exist', () => {
      it('fails', () => {
        const conditionString = 'equals(name, abc)';
        const value = { name: 'abc' };

        expect(() => filter(conditionString)([value])).to.throw(AQLError, "Operator 'equals' not found");
      });
    });

    describe('on strings', () => {
      const abc = { name: 'abc' };
      const def = { name: 'def' };
      const ghi = { name: 'ghi' };
      const testCollection = [abc, def, ghi];

      it('eq', () => {
        const conditionString = 'eq(name, def)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([def]);
      });

      it('eq case Insensitive', () => {
        const conditionString = 'eq(name, DEF, true)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([def]);
      });

      it('ne', () => {
        const conditionString = 'ne(name, def)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([abc, ghi]);
      });

      it('in', () => {
        const conditionString = 'in(name, abc, def)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([abc, def]);
      });

      it('out', () => {
        const conditionString = 'out(name, abc, def)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([ghi]);
      });

      it('gt', () => {
        const conditionString = 'gt(name, def)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([ghi]);
      });

      it('ge', () => {
        const conditionString = 'ge(name, def)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([def, ghi]);
      });

      it('lt', () => {
        const conditionString = 'lt(name, def)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([abc]);
      });

      it('le', () => {
        const conditionString = 'le(name, def)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([abc, def]);
      });

      it('startsWith', () => {
        const conditionString = 'startsWith(name, de)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([def]);
      });

      it('endsWith', () => {
        const conditionString = 'endsWith(name, ef)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([def]);
      });

      it('DEPRECATED: includes without spaces', () => {
        const conditionString = 'includes(name, e)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([def]);
      });

      it('DEPRECATED: includes with spaces', () => {
        const conditionString = 'includes(name, bc de)';
        const matchingValue = { name: 'abc def' };
        const nonMatchingValue = { name: 'ghi' };
        const customTestCollection = [matchingValue, nonMatchingValue];

        const filtered = filter(conditionString)(customTestCollection);
        expect(filtered).to.deep.equal([matchingValue]);
      });

      it('contains without spaces', () => {
        const conditionString = 'contains(name, e)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([def]);
      });

      it('contains with spaces', () => {
        const conditionString = 'contains(name, bc de)';
        const matchingValue = { name: 'abc def' };
        const nonMatchingValue = { name: 'ghi' };
        const customTestCollection = [matchingValue, nonMatchingValue];

        const filtered = filter(conditionString)(customTestCollection);
        expect(filtered).to.deep.equal([matchingValue]);
      });
    });

    describe('on numbers', () => {
      const num123 = { number: 123 };
      const num456 = { number: 456 };
      const num789 = { number: 789.0 };
      const testCollection = [num123, num456, num789];

      it('eq', () => {
        const conditionString = 'eq(number, 456)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([num456]);
      });

      it('eq: float', () => {
        const conditionString = 'eq(number, 789.0)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([num789]);
      });

      it('ne', () => {
        const conditionString = 'ne(number, 456)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([num123, num789]);
      });

      it('in', () => {
        const conditionString = 'in(number, 123, 456)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([num123, num456]);
      });

      it('gt', () => {
        const conditionString = 'gt(number, 456)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([num789]);
      });

      it('ge', () => {
        const conditionString = 'ge(number, 456)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([num456, num789]);
      });

      it('lt', () => {
        const conditionString = 'lt(number, 456)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([num123]);
      });

      it('le', () => {
        const conditionString = 'le(number, 456)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([num123, num456]);
      });

      it('DEPRECATED: includes', () => {
        const conditionString = 'includes(number, 5)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([num456]);
      });

      it('contains', () => {
        const conditionString = 'contains(number, 5)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([num456]);
      });
    });

    describe('on booleans', () => {
      const boolTrue = { bool: true };
      const boolNull = { bool: null };
      const boolFalse = { bool: false };
      const testCollection = [boolTrue, boolNull, boolFalse];

      it('eq: true', () => {
        const conditionString = 'eq(bool, true)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([boolTrue]);
      });

      it('eq: false', () => {
        const conditionString = 'eq(bool, false)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([boolFalse]);
      });

      it('eq: null (falsy)', () => {
        const conditionString = 'eq(bool, null)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([boolNull, boolFalse]);
      });

      it('eq: 0 (falsy)', () => {
        const conditionString = 'eq(bool, 0)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([boolFalse]);
      });

      it('ne: true', () => {
        const conditionString = 'ne(bool, true)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([boolNull, boolFalse]);
      });

      it('ne: false', () => {
        const conditionString = 'ne(bool, false)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([boolTrue, boolNull]);
      });

      it('ne: null (falsy)', () => {
        const conditionString = 'ne(bool, null)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([boolTrue]);
      });

      it('ne: 0 (falsy)', () => {
        const conditionString = 'ne(bool, 0)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([boolTrue, boolNull]);
      });

      it('in', () => {
        const conditionString = 'in(bool, true, false)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([boolTrue, boolFalse]);
      });

      it('gt: true', () => {
        const conditionString = 'gt(bool, true)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([]);
      });

      it('gt: false', () => {
        const conditionString = 'gt(bool, false)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([boolTrue]);
      });

      it('ge: true', () => {
        const conditionString = 'ge(bool, true)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([boolTrue]);
      });

      it('ge: false', () => {
        const conditionString = 'ge(bool, false)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([boolTrue, boolFalse]);
      });

      it('lt: true', () => {
        const conditionString = 'lt(bool, true)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([boolFalse]);
      });

      it('lt: false', () => {
        const conditionString = 'lt(bool, false)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([]);
      });

      it('le: true', () => {
        const conditionString = 'le(bool, true)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([boolTrue, boolFalse]);
      });

      it('le: false', () => {
        const conditionString = 'le(bool, false)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([boolFalse]);
      });
    });

    describe('on arrays', () => {
      const all = { array: [{ name: 'abc' }, { name: 'abc' }, { name: 'abc' }] };
      const any = { array: [{ name: 'abc' }, { name: 'def' }, { name: 'ghi' }] };
      const none = { array: [{ name: 'ghi' }, { name: 'ghi' }, { name: 'ghi' }] };
      const testCollection = [all, any, none];

      it('all', () => {
        const conditionString = 'all(array.*, obj, eq(obj.name,abc))';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([all]);
      });

      it('any', () => {
        const conditionString = 'any(array.*, obj, eq(obj.name,abc))';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([all, any]);
      });

      it('DEPRECATED: includes: deep array', () => {
        const conditionString = 'includes(array.*.name, abc)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([all, any]);
      });

      it('DEPRECATED: includes: simple array', () => {
        const conditionString = 'includes(array, abc)';
        const stringValue = { array: ['abc', 'def', 'ghi'] };
        const numberValue = { array: [123, 456, 789] };
        const customTestCollection = [stringValue, numberValue];

        const filtered = filter(conditionString)(customTestCollection);
        expect(filtered).to.deep.equal([stringValue]);
      });

      it('includesItem: simple array', () => {
        const conditionString = 'includesItem(array, abc)';
        const stringValue = { array: ['abc', 'def', 'ghi'] };
        const numberValue = { array: [123, 456, 789] };
        const customTestCollection = [stringValue, numberValue];

        const filtered = filter(conditionString)(customTestCollection);
        expect(filtered).to.deep.equal([stringValue]);
      });

      it('in', () => {
        const conditionString = 'in(array.*.name, abc)';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([all, any]);
      });

      describe('when a binding has already been set', () => {
        it('fails', () => {
          const conditionString = 'all(array.*, obj, all(foo.*, obj, eq(obj.name, abc)))';

          expect(() => filter(conditionString)(testCollection)).to.throw(AQLError, 'binding already set: obj');
        });
      });
    });

    describe('logical expressions', () => {
      const abc123 = { name: 'abc', number: 123 };
      const def456 = { name: 'def', number: 456 };
      const ghi789 = { name: 'ghi', number: 789 };
      const testCollection = [abc123, def456, ghi789];

      it('and, binary', () => {
        const conditionString = 'and(eq(name, abc), eq(number, 123))';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([abc123]);
      });

      it('and, arbitrary arity', () => {
        const conditionString = 'and(startsWith(name, a), endsWith(name, c), gt(number, 122), lt(number, 124))';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([abc123]);
      });

      it('or, arbitrary arity', () => {
        const conditionString = 'or(eq(name, abc), eq(number, 456), endsWith(name, something), gt(number, 999))';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([abc123, def456]);
      });
    });

    describe('deeply complex expressions', () => {
      const all = { array: [{ name: 'abc' }, { name: 'abc' }, { name: 'abc' }] };
      const any = { array: [{ name: 'abc' }, { name: 'def' }, { name: 'ghi' }] };
      const none = { array: [{ name: 'ghi' }, { name: 'ghi' }, { name: 'ghi' }] };
      const testCollection = [all, any, none];

      it('passes', () => {
        const conditionString = 'all(array.*, obj, and(startsWith(obj.name, a), or(endsWith(obj.name, c), eq(obj.name, jkl))))';

        const filtered = filter(conditionString)(testCollection);
        expect(filtered).to.deep.equal([all]);
      });
    });
  });

  describe('when there are custom operators', () => {
    const customOperators = ({
      // adding a new operator
      hasKey: new Operator((evaluateSubexpression, collection, options, key) =>
        collection.filter((item) => Object.keys(item).includes(key))),

      // overriding a default operator; does not coerce data types
      eq: new Operator((evaluateSubexpression, collection, options, path, value) =>
        collection.filter((item) =>
          item[path] === value)), // eslint-disable-line security/detect-object-injection

      invalidOperator: 'i am invalid'
    });

    const abc = { name: 'abc', number: 123 };
    const def = { name: 'def', number: 456 };
    const ghi = { name: 'ghi' };
    const testCollection = [abc, def, ghi];

    describe('the custom operator', () => {
      it('filters as expected', () => {
        const conditionString = 'hasKey(number)';

        const filtered = filter(conditionString, customOperators)(testCollection);
        expect(filtered).to.deep.equal([abc, def]);
      });
    });

    describe('original operators', () => {
      it('overrides duplicates', () => {
        it('eq', () => {
          const conditionString = 'eq(number, 456)';

          const filtered = filter(conditionString, customOperators)(testCollection);
          expect(filtered).to.deep.equal([]); // empty because the number in the conditionString is regarded to be a string (not the number 456)
        });
      });

      it('keeps originals', () => {
        it('ne', () => {
          const conditionString = 'ne(name, def)';

          const filtered = filter(conditionString, customOperators)(testCollection);
          expect(filtered).to.deep.equal([abc, ghi]);
        });
      });
    });

    describe('when the operator is not an Operator object', () => {
      it('fails', () => {
        const conditionString = 'invalidOperator(name, abc)';
        const value = { name: 'abc' };

        expect(() => filter(conditionString, customOperators)([value])).to.throw(AQLError, "Operator 'invalidOperator' invalid");
      });
    });
  });
});
